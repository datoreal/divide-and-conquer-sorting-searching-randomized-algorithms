# KARATSUBA
# There are two inputs called num1 and num2
# Then we split those inputs in half each one, having then x = a*10^(n/2)+ b, y = c*10^(n/2)+ d


def karatsuba_multiplication(num1, num2):
    if not isinstance(num1, int) or not isinstance(num2, int):
        return None

    len_num1 = len(str(num1))
    len_num2 = len(str(num2))
    str_num1 = str(num1)
    str_num2 = str(num2)

    length = len_num1
    if len_num1 > len_num2:
        dif = len_num1 - len_num2
        add_zeros = ['0' for i in range(0, dif)]
        str_num2 = ''.join(add_zeros) + str_num2

    elif len_num2 > len_num1:
        dif = len_num2 - len_num1
        add_zeros = ['0' for i in range(0, dif)]
        str_num1 = ''.join(add_zeros) + str_num1
        length = len_num2

    if length % 2 != 0:
        str_num2 = '0' + str_num2
        str_num1 = '0' + str_num1
        length = len(str_num2)

    print(str_num1)
    print(str_num2)
    half_n = int(length / 2)

    a = int(str_num1[:half_n])
    b = int(str_num1[half_n:])
    c = int(str_num2[:half_n])
    d = int(str_num2[half_n:])

    step1 = int(a*c)
    step2 = int(b*d)
    step3 = int(a*c) + int(a*d) + int(b*c) + int(b*d)
    step4 = int(step3 - step1 - step2)

    res = ((10**length)*step1) + ((10**half_n)*step4) + step2
    return res




