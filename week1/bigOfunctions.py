import matplotlib.pyplot as plt
import numpy as np
import numpy

# 100 linearly spaced numbers
x = np.linspace(0, 10, 10)
print(x)

# setting the axes at the centre
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_ylim([0,100])
ax.spines['left'].set_position('zero')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# plot the functions
plt.plot(x, numpy.sqrt(x), 'b', label='sqrt(x)')
plt.plot(x, 10**x, 'g', label='10^x')
plt.plot(x, x**1.5, 'r', label='x^1.5')
plt.plot(x, x**(5/3), 'c', label='x^(5/3)')
plt.plot(x, 2**numpy.sqrt(numpy.log2(x)), 'm', label='2^sqrt(log(x))')

plt.legend(loc='upper left')

# show the plot
plt.show()

