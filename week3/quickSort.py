class CountComparisons:
    def __init__(self, first=False, last=False, random=False):
        self.comparisons_count = 0
        self.first = first
        self.last = last
        self.random = random

    def comparisons(self, unsorted_list):
        if len(unsorted_list) <= 1:
            return unsorted_list
        pivot, pivot_index = self.choose_pivot(unsorted_list)
        if pivot is None:
            return []
        sorted_list = self.partition_pivot(unsorted_list, pivot, pivot_index)
        return sorted_list

    def choose_pivot(self, unsorted_list):
        first_index = 0
        if self.first:
            return unsorted_list[first_index], first_index
        elif self.last:
            last_index = len(unsorted_list) - 1
            self.swap_elements(unsorted_list, first_index, last_index)
            return unsorted_list[first_index], first_index
        elif self.random:
            first_index = 0
            middle_index = self._get_middle_index(unsorted_list)
            last_index = len(unsorted_list) - 1
            median_set = [unsorted_list[first_index], unsorted_list[middle_index], unsorted_list[last_index]]
            median_set.sort()

            switch_index = unsorted_list.index(median_set[1])
            self.swap_elements(unsorted_list, first_index, switch_index)
            return unsorted_list[first_index], first_index
        else:
            return None, 0

    @staticmethod
    def _get_middle_index(array_list):
        if len(array_list) % 2 == 0:
            return int((len(array_list) / 2) - 1)
        else:
            return int(len(array_list) / 2)

    @staticmethod
    def swap_elements(array_list, pos1, pos2):
        array_list[pos1], array_list[pos2] = array_list[pos2], array_list[pos1]

    def partition_pivot(self, unsorted_list, pivot, pivot_index):
        self.comparisons_count += (len(unsorted_list) - 1)
        i = pivot_index + 1
        for j in range(pivot_index + 1, len(unsorted_list)):
            if unsorted_list[j] < pivot:
                self.swap_elements(unsorted_list, i, j)
                i += 1
        self.swap_elements(unsorted_list, pivot_index, i - 1)

        left_list = unsorted_list[:(i-1)]
        sorted_list_left = self.comparisons(left_list)
        unsorted_list[:(i - 1)] = sorted_list_left

        right_list = unsorted_list[i:]
        sorted_list_right = self.comparisons(right_list)
        unsorted_list[i:] = sorted_list_right
        return unsorted_list


def read_file():
    with open('./arrayNumber.txt') as f:
        array = [int(line) for line in f]
    return array


if __name__ == '__main__':
    file_list = read_file()
    counter_first = CountComparisons(first=True)
    sorted_list = counter_first.comparisons(file_list)
    print(counter_first.comparisons_count)

    # counter_last = CountComparisons(last=True)
    # sorted_list = counter_last.comparisons(file_list)
    # print(counter_last.comparisons_count)

    # counter_random = CountComparisons(random=True)
    # sorted_list = counter_random.comparisons(file_list)
    # print(counter_random.comparisons_count)
