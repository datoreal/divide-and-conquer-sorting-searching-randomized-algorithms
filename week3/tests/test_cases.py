import unittest
import pytest
from .. import quickSort


class TestCases(unittest.TestCase):
    def test_pivot_first_element(self):
        input_array = [3, 8, 2, 5, 1, 4, 7, 6]

        expected_res = [1, 2, 3, 4, 5, 6, 7, 8]
        expected_num = 15

        counter = quickSort.CountComparisons(first=True)
        res = counter.comparisons(input_array)
        self.assertEqual(res, expected_res)
        self.assertEqual(counter.comparisons_count, expected_num)

    def test_pivot_last_element(self):
        input_array = [3, 8, 2, 5, 1, 4, 7, 6]

        expected_res = [1, 2, 3, 4, 5, 6, 7, 8]
        expected_num = 15

        counter = quickSort.CountComparisons(last=True)
        res = counter.comparisons(input_array)
        self.assertEqual(res, expected_res)
        self.assertEqual(counter.comparisons_count, expected_num)

    def test_pivot_middle_element(self):
        input_array = [3, 8, 2, 5, 1, 4, 7, 6]

        expected_res = [1, 2, 3, 4, 5, 6, 7, 8]
        expected_num = 15

        counter = quickSort.CountComparisons(random=True)
        res = counter.comparisons(input_array)
        self.assertEqual(res, expected_res)
        self.assertEqual(counter.comparisons_count, expected_num)

if __name__ == '__main__':
    unittest.main()
