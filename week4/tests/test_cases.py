import unittest
import pytest
from .. import kragerMinCut


class TestCases(unittest.TestCase):
    def test_edges(self):
        input_graph = {
            1: [2, 3, 5],
            2: [1, 4],
            3: [1, 5],
            4: [2],
            5: [1, 3]
        }

        expected_res = [[1, 2], [1, 3], [1, 5], [2, 4], [3, 5]]

        graph = kragerMinCut.Graph(input_graph)
        res = graph.edges()
        self.assertEqual(res, expected_res)

    def test_minimum_cut(self):
        input_graph = {
            1: [2, 3, 5],
            2: [1, 4],
            3: [1, 5],
            4: [2],
            5: [1, 3]
        }

        graph = kragerMinCut.Graph(input_graph)
        res = graph.minimum_cut()
        self.assertGreater(len(res), 1)


if __name__ == '__main__':
    unittest.main()
