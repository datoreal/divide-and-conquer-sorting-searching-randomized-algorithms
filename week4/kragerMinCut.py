from random import randint


class Graph:
    def __init__(self, graph_dict={}):
        self.__graph_dict = graph_dict

    def vertices(self):
        return list(self.__graph_dict.keys())

    def edges(self):
        edges = []
        graph = self.__graph_dict
        for vertex in graph:
            for neighbour in graph[vertex]:
                if [vertex, neighbour] not in edges and [neighbour, vertex] not in edges:
                    edges.append([vertex, neighbour])
        return edges

    def minimum_cut(self):
        vertex_list = self.vertices()
        edge_list = self.edges()
        while len(vertex_list) > 2:
            random_edge_index = randint(0, (len(edge_list)-1))
            random_edge = edge_list[random_edge_index]
            replace_with = random_edge[0]
            should_replace = random_edge[1]
            for edge in edge_list:
                if edge[0] == should_replace:
                    edge[0] = replace_with
                if edge[1] == should_replace:
                    edge[1] = replace_with
            edge_list.remove(random_edge)
            vertex_list.remove(should_replace)
            is_not_self_loop = lambda x: x[0] != x[1]
            edge_list = list(filter(is_not_self_loop, edge_list))
        return edge_list


def read_file():
    graph = {}
    with open('./arrayNumber.txt') as f:
        for line in f:
            vertex_id = line.split()[0]
            graph[vertex_id] = line.split()[1:]
    return graph


if __name__ == '__main__':
    file_list = read_file()
    graph = Graph(file_list)
    min_cut = len(graph.minimum_cut())
    for i in range(0, 400):
        graph = Graph(file_list)
        edges = graph.minimum_cut()
        current_cut = len(edges)
        if current_cut < min_cut:
            print(i, ' NEW MIN', current_cut)
            min_cut = current_cut
    print(edges)
