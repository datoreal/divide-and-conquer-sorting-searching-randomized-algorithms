import unittest
import pytest
from .. import numberOfInversion


class TestCases(unittest.TestCase):
    def test_six_numbers_half_sorted(self):
        input_array = [1, 3, 5, 2, 4, 6]

        expected_res = [1, 2, 3, 4, 5, 6]
        expected_num = 3

        counter = numberOfInversion.CountInversions()
        res = counter.inversions(input_array)
        self.assertEqual(res, expected_res)
        self.assertEqual(counter.inversion_count, expected_num)

    def test_six_numbers_not_sorted(self):
        input_array = [1, 6, 8, 3, 7, 5, 4, 2]

        expected_res = [1, 2, 3, 4, 5, 6, 7, 8]
        expected_num = 16

        counter = numberOfInversion.CountInversions()
        res = counter.inversions(input_array)
        self.assertEqual(res, expected_res)
        self.assertEqual(counter.inversion_count, expected_num)

    def test_six_numbers_length_power_three(self):
        input_array = [1, 6, 3, 5, 4, 2]

        expected_res = [1, 2, 3, 4, 5, 6]
        expected_num = 8

        counter = numberOfInversion.CountInversions()
        res = counter.inversions(input_array)
        self.assertEqual(res, expected_res)
        self.assertEqual(counter.inversion_count, expected_num)

    def test_six_numbers_repited_numbers(self):
        input_array = [1, 6, 3, 3, 3, 2]

        expected_res = [1, 2, 3, 3, 3, 6]
        expected_num = 6

        counter = numberOfInversion.CountInversions()
        res = counter.inversions(input_array)
        self.assertEqual(res, expected_res)
        self.assertEqual(counter.inversion_count, expected_num)

if __name__ == '__main__':
    unittest.main()
