class CountInversions:
    def __init__(self):
        self.inversion_count = 0

    def sort_list(self, input_array):
        if len(input_array) % 2 == 0 or (len(input_array) % 2 == 1 and len(input_array) > 1):
            half_array = int(len(input_array) / 2)
            first_halve = self.sort_list(input_array[:half_array])
            second_halve = self.sort_list(input_array[half_array:])
            sorted_array, inversions = self.merge_list_and_count(first_halve, second_halve, count=True)
            self.inversion_count += inversions
            return sorted_array
        else:
            return input_array

    @staticmethod
    def merge_list_and_count(list_a, list_b, count=False):
        i = 0
        j = 0
        equal = False
        n = len(list_a) + len(list_b)
        output = []
        num_inversions = 0
        is_last_a = False
        is_last_b = False
        for k in range(0, n):
            if equal:
                equal = False
                continue
            if list_a[i] < list_b[j]:
                if not is_last_a:
                    output.append(list_a[i])
                    if i < (len(list_a) - 1):
                        i += 1
                    else:
                        is_last_a = True
                else:
                    output.append(list_b[j])
                    if j < (len(list_b) - 1):
                        j += 1
            elif list_a[i] > list_b[j]:
                if not is_last_b:
                    output.append(list_b[j])
                    if j < (len(list_b) - 1):
                        j += 1
                    else:
                        is_last_b = True
                    if count:
                        num_inversions += (len(list_a) - i)
                else:
                    output.append(list_a[i])
                    if i < (len(list_a) - 1):
                        i += 1
            else:
                # list_b[j] == list_a[i]
                equal = True
                if not is_last_a:
                    output.append(list_a[i])
                    if i < (len(list_a) - 1):
                        i += 1
                    else:
                        is_last_a = True
                if not is_last_b:
                    output.append(list_b[j])
                    if j < (len(list_b) - 1):
                        j += 1
                    else:
                        is_last_b = True
        if count:
            return output, num_inversions
        return output

    def inversions(self, array_list):
        if len(array_list) == 1:
            return array_list
        half_array = int(len(array_list) / 2)
        x = self.sort_list(array_list[:half_array])
        y = self.sort_list(array_list[half_array:])
        res, inversions = self.merge_list_and_count(x, y, count=True)
        self.inversion_count += inversions
        return res


def read_file():
    with open('./arrayNumber.txt') as f:
        array = [int(line) for line in f]
    return array


if __name__ == '__main__':
    file_list = read_file()
    counter = CountInversions()
    counter.inversions(file_list)
    print(counter.inversion_count)
